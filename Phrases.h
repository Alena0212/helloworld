#pragma once
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

struct phrase_data
{
	std::string phrase;
	int freq;
};

bool cmp(phrase_data, phrase_data);
int phrases(std::string, int, int);
std::vector<std::string> stream_to_words(std::istream&);
std::map<std::string, int> words_to_counted_phrases(std::vector<std::string>, int);
std::vector<phrase_data> map_to_phrase_data(std::map<std::string, int>);
