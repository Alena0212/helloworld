#include "Phrases.h"

int main(int argc, char* argv[])
{
	int n = 2, m = 2;
	if (argc == 1) {
		std::cerr << "No command arguments" << std::endl;
		getchar();
		return -1;
	}
	if (argc > 6) {
		std::cerr << "Too many arguments" << std::endl;
		getchar();
		return -2;
	}
	int i = 1;
	for (; i < argc - 1; i++) {
		if (std::string(argv[i]) == "-n") {
			n = atoi(argv[++i]);
			continue;
		}
		if (std::string(argv[i]) == "-m") {
			m = atoi(argv[++i]);
			continue;
		}
	}
	int rc = phrases(std::string(argv[i]), n, m);
	getchar();
	return rc;
}