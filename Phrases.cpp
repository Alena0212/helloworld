#include "Phrases.h"

using std::string;
using std::vector;
using std::map;


bool cmp(phrase_data d1, phrase_data d2) {
	return (d1.freq > d2.freq);
}

vector<string> stream_to_words(std::istream& stream) {
	string word;
	vector<string> words;
	while (stream >> word) words.push_back(word);
	return words;
}

map<string, int> words_to_counted_phrases(vector<string> words, int n) {
	map<string, int> stats;
	for (unsigned int i = 0; i < words.size() - n + 1; i++) {
		string phrase;
		for (int j = i; j < n + i; j++)
			phrase += words[j] + ((j == n + i - 1) ? (' ') : ('\0'));
		if (stats.count(phrase) > 0) stats[phrase]++;
		else stats.insert(std::pair<string, int>(phrase, 1));
	}
	return stats;
}

vector<phrase_data> map_to_phrase_data(map<string, int> stats) {
	vector<phrase_data> v;
	for (auto p = stats.begin(); p != stats.end(); ++p) {
		phrase_data tmp;
		tmp.phrase = (*p).first;
		tmp.freq = (*p).second;
		v.push_back(tmp);
	}
	return v;
}

int phrases(string sname, int n, int m) {
	vector<string> words;
	if (sname == "-")	{
	words = stream_to_words(std::cin);
	}
	else {
		std::ifstream file_in(sname);
		if (!file_in.is_open()) {
			std::cerr << "Error opening file " << sname << std::endl;
			return -1;
			}
	words =	stream_to_words(file_in);
	}

	if (words.size() < n) {
		std::cerr << "n is more than the amount of words in input" << std::endl;
		return -2;
	}

	map<string, int> stats = words_to_counted_phrases(words, n);

	vector<phrase_data> data = map_to_phrase_data(stats);
	
	sort(data.begin(), data.end(), cmp);

	for (unsigned int i = 0; ((i < data.size()) && (data[i].freq >= m)); i++)
		std::cout << data[i].phrase << '(' << data[i].freq << ')' << std::endl;
	return 0;
}

